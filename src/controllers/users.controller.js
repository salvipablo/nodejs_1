import {
  allUsers,
  add,
  update,
  erase,
  login,
  register,
} from "../models/users.model.js";

export const getAllUsers = async (_req, res) => {
  let operationAllUsers = await allUsers();

  let statusCode = operationAllUsers.success ? 200 : 404;

  return res.status(statusCode).json({
    message: operationAllUsers.message,
    data: operationAllUsers.data
  });
};

export const createUser = async (req, res) => {
  let operationCreate = await add(req.body);

  let statusCode = operationCreate.success ? 201 : 500;

  return res.status(statusCode).json({
    message: operationCreate.message,
    data: operationCreate.data
  });
};

export const updateUser = async (req, res) => {
  let operationUpdate = await update(req.params.id, req.body);

  let statusCode = operationUpdate.success ? 200 : 500;

  return res.status(statusCode).json({
    message: operationUpdate.message,
    data: operationUpdate.data
  });
};

export const deleteUser = async (req, res) => {
  let operationDelete = await erase(req.params.id);

  let statusCode = operationDelete.success ? 200 : 500;

  return res.status(statusCode).json({
    message: operationDelete.message,
    data: operationDelete.data
  });
};

export const registerUser = async (req, res) => {
  let operationRegister = await register(req.userToRegister);

  let statusCode = operationRegister.success ? 201 : 500;

  return res.status(statusCode).json({
    message: operationRegister.message,
    data: operationRegister.data
  });
};

export const loginUser = async (req, res) => {
  let operationLogin = await login(req.userToLogin);

  let statusCode = operationLogin.success ? 200 : 500;

  return res.status(statusCode).json({
    message: operationLogin.message,
    data: operationLogin.data
  });
};
