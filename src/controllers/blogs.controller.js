import {
  allBlogs,
  newBlog,
  profile,
  editProfile
} from "../models/blogs.model.js";

export const getAllBlogs = async (req, res) => {
  let operationAllblogs = await allBlogs(req.usernameReq);

  let statusCode = operationAllblogs.success ? 200 : 500;

  return res.status(statusCode).json({
    message: operationAllblogs.message,
    data: operationAllblogs.data
  });
};

export const createNewBlog = async (req, res) => {
  let operationCreateblog = await newBlog(req.usernameReq, req.body);

  let statusCode = operationCreateblog.success ? 200 : 500;

  return res.status(statusCode).json({
    message: operationCreateblog.message,
    data: operationCreateblog.data
  });
};

export const getProfile = async (req, res) => {
  let operationGetProfile = await profile(req.params.id);

  let statusCode = operationGetProfile.success ? 200 : 404;

  return res.status(statusCode).json({
    message: operationGetProfile.message,
    data: operationGetProfile.data
  });
};

export const editProfileUser = async (req, res) => {
  let operationEditProfile = await editProfile(req.params.id);

  let statusCode = operationEditProfile.success ? 200 : 401;

  return res.status(statusCode).json({
    message: operationEditProfile.message,
    data: operationEditProfile.data
  });
};
