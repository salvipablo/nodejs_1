import redis from 'redis';
import 'dotenv/config';

let redisClient;

(async () => {
  //console.log('Module: redis.js -- Meesage: Funcion autollamada');
  redisClient= redis.createClient();
  redisClient.on("error", (error) => { console.error(`Error: ${error}`) });
  await redisClient.connect(process.env.REDIS_URI);
})();

//console.log('Module: redis.js -- Meesage: Sigo la ejecucion despues de connect en funcion autollamada');

export default redisClient;