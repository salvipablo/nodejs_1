import mongoose from "mongoose";
import 'dotenv/config';

const { DB_NAME, LOC_DB } = process.env;

mongoose.connect(`mongodb://${LOC_DB}/${DB_NAME}`, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

mongoose.connection.on('error', () => {
  console.error('Error de conexion con la base de datos.')
});

mongoose.connection.once('open', () => {
  console.log('MongoDB: Conexión exitosa con la base de datos.');
});

export default mongoose;
