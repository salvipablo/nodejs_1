import 'dotenv/config';

import redisClient from "../config/redis.js";
import User from "./user.model.js";


export const allBlogs = async (username) => {
  try {
    let blogs = await User.findOne({ username }).select('blogs');

    return {
      message: 'Sending of all blogs of the database',
      data: blogs,
      success: true
    };
  } catch (error) {
    return {
      message: error.message,
      data: [],
      success: false
    };
  }
};

export const newBlog = async (username, dataForNewBlog) => {
  try {
    const newBlog = await User.updateOne(
      {
        "username": username,
      },
      {
        $push: {
          "blogs": {
            "title": dataForNewBlog.title,
            "description": dataForNewBlog.description,
            "image": dataForNewBlog.image,
            "active": dataForNewBlog.active
          }
        }
      }
    );

    return {
      message: 'Create blog sucessfully',
      data: newBlog,
      success: true
    };
  } catch (error) {
    return {
      message: error.message,
      data: [],
      success: false
    };
  }
};

export const profile = async (idUser) => {
  try {
    let user = await User.findOne({ _id: idUser })
                                            .select('firstname lastname email username userlevel active blogs.title');

    const userCache = JSON.stringify({
      firstname: user.firstname,
      lastname: user.lastname
    });

    let dataResponse = {};
    let data = await redisClient.get(idUser);

    if (!data) {
      console.log('Devuelto por la base de datos');
      redisClient.set(idUser, userCache, {EX: parseInt(process.env.REDIS_TTL) });
      dataResponse = JSON.parse(userCache)
    } else {
      console.log('Devuelto por la cache');
      dataResponse = JSON.parse(data);
    }

    return {
      message: 'Sending profile information',
      data: dataResponse,
      success: true
    };
  } catch (error) {
    return {
      message: error.message,
      data: {},
      success: false
    };
  }
};

export const editProfile = async (idUser) => {
  try {
    redisClient.del(idUser);

    return {
      message: 'Edit Profile',
      data: {},
      success: true
    };
  } catch (error) {
    return {
      message: error.message,
      data: {},
      success: false
    };
  }  
};
