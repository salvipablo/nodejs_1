import mongoose from "../config/mongo.config.js";

const userSchema = new mongoose.Schema(
  {
    firstname: String,
    lastname: String,
    email: String,
    username: String,
    password: String,
    userLevel: Number,
    active: Boolean,
    blogs: [
      {
        title: String,
        description: String,
        image: String,
        active: Boolean
      }
    ]
  }, {
    timestamps: true
  }
);

const User = mongoose.model("users", userSchema);

export default User;
