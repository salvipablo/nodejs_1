import bcrypt from 'bcrypt';
import 'dotenv/config';
import jwt from 'jsonwebtoken';

import User from "./user.model.js";

const { TOKEN_SECRET } = process.env;

export const allUsers = async () => {
  try {
    let users = await User.find({});

    if (users.length === 0) {
      throw new Error('No users were found in the database.')
    }

    return {
      message: 'Sending of all users of the database',
      data: users,
      success: true
    };
  } catch (error) {
    return {
      message: error.message,
      data: [],
      success: false
    };
  }
};

export const add = async (dataForUser) => {
  try {
    let createUser = await User.create(dataForUser);

    return {
      message: 'User created successfully',
      data: createUser,
      success: true
    };
  } catch (error) {
    return {
      message: error.message,
      data: {},
      success: false
    };
  }
};

export const update = async (idUser, dataUpdateForUser) => {
  try {
    let updateUser = await User.findByIdAndUpdate({ _id: idUser }, dataUpdateForUser);

    return {
      message: 'User updated successfully',
      data: updateUser,
      success: true
    };
  } catch (error) {
    return {
      message: error.message,
      data: {},
      success: false
    };
  }
};

export const erase = async (idUser) => {
  try {
    let deleteUser = await User.findByIdAndDelete({ _id: idUser });

    if (!deleteUser) {
      throw new Error('The user you are trying to delete does not exist in the database');
    }

    return {
      message: 'User successfully deleted',
      data: deleteUser,
      success: true
    };
  } catch (error) {
    return {
      message: error.message,
      data: {},
      success: false
    };
  }
};

export const register = async (dataForRegister) => {
  try {
    const salt = await bcrypt.genSaltSync(10);
    const passwordHashed = await bcrypt.hash(dataForRegister.password, salt);

    dataForRegister.password = passwordHashed;

    let registerUser = await User.create(dataForRegister);

    return {
      message: 'User register successfully',
      data: registerUser,
      success: true
    };
  } catch (error) {
    return {
      message: error.message,
      data: {},
      success: false
    };
  }
};

export const login = async (dataForLogin) => {
  try {
    const { username, password } = dataForLogin;

    let login = await User.findOne({ username });

    if (!login) {
      throw new Error('The user trying to log in does not exist in the database');
    }

    const compare = await bcrypt.compare(password, login.password);

    if (!compare) {
      throw new Error('The password entered is incorrect');
    }

    let infoUser = {
      id: login._id,
      username: login.username,
      active: login.active,
      userLevel: login.userLevel
    };

    const token = jwt.sign(
      infoUser,
      TOKEN_SECRET
    );

    return {
      message: 'Permission granted',
      data: token,
      success: true
    };
  } catch (error) {
    return {
      message: error.message,
      data: {},
      success: false
    };
  }
};