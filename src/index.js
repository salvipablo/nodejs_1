import express from "express";
import cors from 'cors';
import 'dotenv/config';

import userRouter from "./routes/users.routes.js";
import blogRouter from "./routes/blogs.routes.js";

//import redisClient from "./config/redis.js";

const app = express();
const { PORT } = process.env;

// Midllewares.
app.use(cors());
app.use(express.json());

// Routes.
app.use('/users', userRouter);
app.use('/blogs', blogRouter);

// Server start.
app.listen(PORT, () => {
  try {
    console.clear();

    console.log(`Server listening on port: ${PORT}`);
  } catch (error) {
    console.log(`Server unable to start. Failed with the following message: ${error.message}`);
  }
});
