import Joi from "joi";

const schemaCreateUser = Joi.object(
  {
    firstname: Joi.string()
    .trim()
    .required(),

    lastname: Joi.string()
    .trim()
    .required(),

    email: Joi.string()
    .trim()
    .email()
    .required(),

    username: Joi.string()
    .trim()
    .min(6)
    .max(30)
    .alphanum()
    .required(),

    password: Joi.string()
    .min(8)
    .max(30)
    .alphanum()
    .required(),
  }
);

// userLevel: Joi.number()
// .required(),

// active: Joi.boolean()
// .required(),

// blogs: Joi.array()


export const validationUser = (req, res, next) => {
  let dataValidate = {
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    email: req.body.email,
    username: req.body.username,
    password: req.body.password,
  };

  const { error } = schemaCreateUser.validate(dataValidate)

  if (error) {
    return res.status(400).json({
      message: 'Validations have not been passed',
      data: error.details
    });
  }

  dataValidate.userLevel = 2;
  dataValidate.active = true;
  dataValidate.blogs = [];

  req.userToRegister = dataValidate;

  next();
};
