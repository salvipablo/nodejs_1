import jwt from 'jsonwebtoken';
import 'dotenv/config';

const { TOKEN_SECRET } = process.env;

export const validationBlog = async (req, res, next) => {
  let bearerToken = req.header('authorization');

  if (!bearerToken) {
    return res.status(401).json({
      message: 'You are not authorized to receive a response to this request',
      data: []
    });
  }

  let token = bearerToken.split(' ')[1];

  let infoUser = await dataFromToken(token);

  if (!infoUser.username) {
    return res.status(401).json({
      message: 'You are not authorized to receive a response to this request',
      data: []
    });
  }

  req.usernameReq = infoUser.username;

  next();
};

const dataFromToken = async (token) => {
  return jwt.verify(token, TOKEN_SECRET, (err, data) => {
    if (err) return err;
    return data;
  });
};
