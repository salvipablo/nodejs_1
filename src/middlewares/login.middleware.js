import Joi from "joi";

const schemaLogin = Joi.object(
  {
    username: Joi.string()
    .trim()
    .min(6)
    .max(30)
    .alphanum()
    .required(),

    password: Joi.string()
    .min(8)
    .max(30)
    .alphanum()
    .required()
  }
);

export const validationLogin = (req, res, next) => {
  const { error, value } = schemaLogin.validate(req.body)

  if (error) {
    return res.status(400).json({
      message: 'Validations have not been passed',
      data: error.details
    });
  }

  req.userToLogin = value;

  next();
};
