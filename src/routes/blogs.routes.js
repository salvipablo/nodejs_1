import express from "express";

import {
  getAllBlogs,
  createNewBlog,
  getProfile,
  editProfileUser
} from "../controllers/blogs.controller.js";

import {
  validationBlog,
} from "../middlewares/blogs.middleware.js";

const blogRouter = express.Router();

blogRouter.get('/', validationBlog, getAllBlogs);
blogRouter.get('/create', validationBlog, createNewBlog);
blogRouter.get('/profile/:id', validationBlog, getProfile);
blogRouter.put('/profile/:id', validationBlog, editProfileUser);

export default blogRouter;