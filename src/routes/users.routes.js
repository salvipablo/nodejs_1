import express from "express";

import {
  getAllUsers,
  createUser,
  updateUser,
  deleteUser,
  loginUser,
  registerUser,
} from "../controllers/users.controller.js";

import {
  validationUser,
} from "../middlewares/user.middleware.js";

import {
  validationLogin,
} from "../middlewares/login.middleware.js";

const userRouter = express.Router();

userRouter.get('/', getAllUsers);
userRouter.post('/', createUser);
userRouter.put('/:id', updateUser);
userRouter.delete('/:id', deleteUser);

userRouter.post('/register/', validationUser, registerUser);
userRouter.post('/login/', validationLogin, loginUser);

export default userRouter;
